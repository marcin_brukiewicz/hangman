const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production'; // based on the constiable set in the terminal
const cssDev = ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'];
const cssProd = ExtractTextPlugin.extract({
  use: ['css-loader', 'postcss-loader', 'sass-loader'],
  fallback: 'style-loader'
});
const cssConfig = isProd ? cssProd : cssDev;

module.exports = {
  entry: {
    'vendor': ['react', 'react-dom','jquery'],
    app: path.join(__dirname, 'src', 'app.js')
  },
  output: {
    filename: '[name].[chunkhash].bundle.js',
    path: path.join(__dirname, 'dist')
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        use: 'babel-loader',
        exclude: /(node_modules|bower_components)/,
      },
      {
        test: /\.(png|svg|jpe?g|gif)$/i,
        use: [
          'file-loader?name=[name].[ext]&outputPath=images/', // The options passed after '?' preserve the path structure in production (so we get a e.g dist/images/background.PNG instead of just dist/background.PNG)
          'image-webpack-loader'
        ]
      },
      // { test: /\.ttf$/, loader: 'file-loader?name=fonts/[name].[ext]' },
      { test: /\.ttf$/, loader: 'url-loader?limit=100000' },
      {
        test: /\.s?css$/,
        use: cssConfig
      },
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"), // Server files from this folder
    compress: true, // Use gzip compression
    hot: true,
    stats: "errors-only", // Display only errors while running the webpack-dev-server
    open: false // Opens new window in the browser atutomatically after webpack-dev-server starts
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
    }),
    new HtmlWebpackPlugin({
      title: 'My project',
      minify: {
        collapseWhitespace: true
      },
      hash: true,
      chunks: ['vendor', 'app'],
      filename: 'index.html', // Run simple 'webpack' command to generate it (running webpack-deb-server won't generate this file)
      template: './src/templates/index.ejs'
    }),
    new ExtractTextPlugin({
      filename: 'css/[name].css',
      disable: !isProd, // Turning it on/off based on the environment #HMR
      allChunks: true
    }),
    new webpack.HotModuleReplacementPlugin(), // enable hot module replacement #HMR
    new webpack.NamedModulesPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      names: [
        'vendor' // This extracts what is defined under 'vendor' in the entry point
      ],
      filename: '[name].[hash].js' // Needed for [chunkhash] to work in the output option -- http://tips.tutorialhorizon.com/2016/10/31/cannot-use-chunkhash-for-chunk-in-name-chunkhash-js-use-hash-instead/
    })
  ]
}
