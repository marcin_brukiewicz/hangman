import React, { Component } from 'react';
import Transition from 'react-motion-ui-pack';

const Errors = ({error}) => (
    <div className="errors" key={'1'}>
      <Transition
        component={false}
        measure={false}
        enter={{opacity: 1}}
        leave={{opacity: 0}}
      >
      {error !== null &&
        <div key={'error'}>
          <h3>Oops!</h3>
          <p>{error}</p>
        </div>
      }
      </Transition>
    </div>
);

export default Errors;
