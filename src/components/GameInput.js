import React, { Component } from 'react';

const Box = ({letter, active}) => (
  <div className="input-box__wrapper flex-1">
    <div className="aspect"></div>
    <div className={`input-box flex-column br-2 ${active ? 'active' : ''}`}>
      <p>{letter}</p>
    </div>
  </div>
)

const GameInput = ({ numberOfBoxes, mappedGameWord }) => {
  const emptyBoxes = [...Array(numberOfBoxes - mappedGameWord.length)]
  return (
    <div className="game-input flex-row">
      {emptyBoxes.map((x, i) => <Box key={i}/>)}
      {mappedGameWord.map((x, i) => {
        const letter = x.typed ? x.letter : '';
        return <Box key={i} letter={letter} active/>
      })}

    </div>
  );
}

GameInput.defaultProps = {
  numberOfBoxes: 11
}

export default GameInput;
