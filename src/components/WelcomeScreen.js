import React from 'react';

const WelcomeScreen = ({startGame}) => {
  return (
    <div className="welcome-screen">
      <img src={require('../images/1-head.png')} />
      <h1>Hey there, this is Hangman!</h1>
      <p>Use your keyboard to type desired characters</p>
      <p>Start your game whenever you like!</p>
      <button className="hangman_button" onClick={startGame}>Start</button>
    </div>
  );
}

export default WelcomeScreen;
