import React, { Component } from 'react';

const importAll = (r) => {
  let images = {};
  r.keys().map((item, index) => { images[item.replace(/\.\/[1-9]\-|\.png/g, '')] = r(item); });
  return images;
}
const images = importAll(require.context('../images', false, /[1-9]\-\w+\.png$/));
const getBodyparts = () => {
  const bodyparts = [];
  const pairs = ['arm', 'hand', 'leg', 'foot'];
  let id = 0;
  Object.keys(images).map(name => {
    const path = images[name];
    const hidden = true;
    if (pairs.includes(name)) {
      bodyparts.push({id, name, path, reverse: true, hidden});
      id += 1;
    }
    bodyparts.push({
      id,
      name,
      path,
      reverse: false,
      hidden
    });
    id += 1;
  });
  return bodyparts;
}

const TheHangman = ({ missedLetters, bodyparts }) => {
  const mappedParts = bodyparts.map(part => {
    part.hidden = !(part.id <= missedLetters.length - 1);
    return part;
  });
  return (
    <div className="the-hangman half-width">
      <img src={require('../images/bar.png')} className="bar" />
      {mappedParts.map((part, i) => {
        const reverseClass = part.reverse ? 'reverse' : '';
        const hiddenClass = part.hidden ? 'hidden' : '';
        return <img src={part.path} key={i} className={`bodypart ${part.name} ${reverseClass} ${hiddenClass}`} />
      })}
    </div>
  );
}

TheHangman.defaultProps = {
  bodyparts: getBodyparts(),
}

export default TheHangman;
