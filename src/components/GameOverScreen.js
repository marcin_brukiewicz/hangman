import React, { Component } from 'react';

const WinMessage = () => (
  <div className="message--win">
    <h3>Whoah!</h3>
    <p>You won! Congratulations!</p>
  </div>
);
const LostMessage = ({word}) => (
  <div className="message--lost">
    <h3>Ooops</h3>
    <p>Seems like it didn't go well. Try again!</p>
    <p>The word that let him hang:<br/><span>{word}</span></p>
  </div>
)

const GameOverScreen = ({ gameResult, word, resetGame }) => {
  const messageComponent = gameResult == 'won' ?
    <WinMessage /> :
    <LostMessage word={word} />;
  return (
    <div className="gameover-screen">
      <h1>GAME OVER</h1>
      {messageComponent}
      <button className="hangman_button yellow" onClick={() => resetGame(word)}>NEW WORD</button>
    </div>
  )
}

export default GameOverScreen;
