import React, { Component } from 'react';
import { DoubleBounce } from 'better-react-spinkit';

const LoadingScreen = (props) => (
  <div className="loading-screen">
    <DoubleBounce color="#ffac00" size={50}/>
    <h3>Hold on! We are getting you are new word!</h3>
  </div>
)

export default LoadingScreen;
