import React, { Component } from 'react';


const MissedLetters = ({ missedLetters }) => (
  <div className="missed-letters__wrapper flex-column full-width">
    <p>YOU MISSED:</p>
    <div className="missed-letters flex-row">
      {missedLetters.map( (letter, i) => {
        return <p key={i}>{letter}</p>
      })}
    </div>
  </div>
);


export default MissedLetters;
