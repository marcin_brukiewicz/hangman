require('./styles/app.scss');
// import classNames from 'classnames'; // remove the dependency

import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';

ReactDOM.render(
  <App />,
  document.getElementById('app')
)
