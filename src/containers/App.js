import React, { Component } from 'react';
import { Provider } from 'react-redux';
import reduxSetup from '../redux/setup';
import RootContainer from './RootContainer';


const store = reduxSetup();

const App = (props) => (
  <Provider store={store}>
    <RootContainer />
  </Provider>
);

export default App;
