import React, { Component } from 'react';
import { connect } from 'react-redux';
import HangmanActions from '../redux/reducers/hangman';
import WelcomeScreen from '../components/WelcomeScreen';
import GameOverScreen from '../components/GameOverScreen';
import GameInput from '../components/GameInput';
import MissedLetters from '../components/MissedLetters';
import TheHangman from '../components/TheHangman';
import Errors from '../components/Errors';
import LoadingScreen from '../components/LoadingScreen';

class RootContainer extends Component {
  componentDidMount() {
    this.captureKeypress();
  }

  captureKeypress() {
    window.addEventListener('keypress', (event) => {
      event = event || window.event;
      event.preventDefault();
      const keyCode = event.keyCode || event.which ;
      if (!keyCode) return;
      const char = String.fromCharCode(keyCode).toUpperCase();
      const { displayError, hangman } = this.props;
      if (hangman.missedLetters.includes(char)) {
        displayError('You have already missed this character!');
      } else if (hangman.caughtLetters.includes(char)) {
        displayError('Look down! It\'s already there! :)');
      } else if (!char.match(/[A-Z]|\-/g)) {
        displayError('This character is not allowed!');
      } else {
        this.props.keyPressed(char);
      }
      this.catchGameEnd();
    }, false);
  }

  catchGameEnd() {
    // Steps to end the game: 11
    const { missedLetters, mappedGameWord } = this.props.hangman;
    const { endGame } = this.props;
    let result = null;
    if (missedLetters.length == 11) result = "lost";
    if (mappedGameWord.every(e => e.typed)) result = "won";
    if (result) endGame(result);
  }

  render() {
    const { hangman: {
      word,
      fetching,
      missedLetters,
      mappedGameWord,
      error,
      gameStarted,
      gameResult,
    }} = this.props;
    const { startGame, resetGame } = this.props;
    if (!gameStarted && !word) {
      return <WelcomeScreen startGame={startGame}/>;
    }
    return (
      <div className="game-box modern-shadow fix-center flex-column br-3">
        {fetching && <LoadingScreen />}
        {gameResult && <GameOverScreen word={word} gameResult={gameResult} resetGame={resetGame} />}
        <div className="flex-row flex-1">
          <TheHangman missedLetters={missedLetters} />
          <div className="flex-column flex-1 half-width">
            <MissedLetters missedLetters={missedLetters}/>
            <Errors error={error} />
          </div>
        </div>
        <GameInput mappedGameWord={mappedGameWord}/>
        <div className="triangle"></div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  hangman: state.hangman
});

const mapDispatchToProps = (dispatch) => ({
  getRandomWord: () => dispatch(HangmanActions.wordRequest()),
  keyPressed: (character) => dispatch(HangmanActions.keyPressed(character)),
  displayError: (error) => dispatch(HangmanActions.displayError(error)),
  startGame: () => dispatch(HangmanActions.gameStart()),
  endGame: (result) => dispatch(HangmanActions.gameEnd(result)),
  resetGame: (previousWord) => dispatch(HangmanActions.gameReset(previousWord))
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
