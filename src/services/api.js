import apisauce from 'apisauce';

const create = (baseURL = 'http://api.wordnik.com:80/v4/') => {
  const api = apisauce.create({
    baseURL,
    timeout: 3000 // 3s timeout
  });
  const api_key = 'a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5';
  const getRandomWord = (wordType = 'noun') => {
    return api.get('words.json/randomWord', {
      hasDictionaryDef: false,
      includePartOfSpeech: wordType,
      minCorpusCount: 0,
      maxCorpusCount: -1,
      minDictionaryCount: 1,
      maxDictionaryCount: -1,
      minLength: 5,
      maxLength: 11,
      api_key
    })
  }
  return {
    getRandomWord,
  }
}

export default {
  create
}
