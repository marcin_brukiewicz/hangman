import { combineReducers } from 'redux';
import rootSaga from './sagas';
import storeSetup from './store';


const reduxSetup = () => {
  const rootReducer = combineReducers({
    hangman: require('./reducers/hangman').reducer,
  })

  return storeSetup(rootReducer, rootSaga);
}

export default reduxSetup;
