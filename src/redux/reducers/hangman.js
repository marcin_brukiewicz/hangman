import { createReducer, createActions } from 'reduxsauce';
import { keyPressedFormatter } from '../formatters/hangman';

const { Types, Creators } = createActions({
  wordRequest: ['previousWord'],
  wordSuccess: ['word'],
  wordFailure: null,
  keyPressed: ['char'],
  displayError: ['error'],
  gameStart: null,
  gameEnd: ['gameResult'],
  gameReset: ['previousWord']
});

export const HangmanTypes = Types;
export default Creators;

export const INITIAL_STATE = {
  word: null,
  mappedGameWord: [],
  fetching: null,
  error: null,
  missedLetters: [],
  caughtLetters: [],
  gameStarted: false,
  gameResult: null
}

export const gameStart = (state) =>
  Object.assign({}, state, { gameStarted: true, fetching: true })


export const gameEnd = (state, { gameResult }) =>
  Object.assign({}, state, { gameStarted: false, gameResult})

export const gameReset = (state) =>
  Object.assign({}, state, {
    gameStarted: true,
    fetching: true,
    mappedGameWord: [],
    missedLetters: [],
    caughtLetters: [],
    gameResult: null,
    word: null
  })


export const request = (state) =>
  Object.assign({}, state, { fetching: true })

export const success = (state, { word }) => {
  const mappedGameWord = word.split('').map((letter, i) => {
    return {
      id: i,
      letter,
      typed: false
    }
  })
  return Object.assign({}, state, { word, mappedGameWord, fetching: false })
}

export const failure = (state) =>
  Object.assign({}, state, { error: true })

export const keyPressed = (state, { char }) => {
  return keyPressedFormatter(state, char);
}

export const displayError = (state, { error }) => {
  return Object.assign({}, state, {
    error
  })
}


export const reducer = createReducer(INITIAL_STATE, {
  [Types.WORD_REQUEST]: request,
  [Types.WORD_SUCCESS]: success,
  [Types.WORD_FAILURE]: failure,
  [Types.KEY_PRESSED]: keyPressed,
  [Types.DISPLAY_ERROR]: displayError,
  [Types.GAME_START]: gameStart,
  [Types.GAME_END]: gameEnd,
  [Types.GAME_RESET]: gameReset
});
