import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';


const storeSetup = (rootReducer, rootSaga) => {
  // Middlewares
  const middlewares = [];
  if (process.env.NODE_ENV !== 'production') middlewares.push(logger);
  const sagaMiddleware = createSagaMiddleware();
  middlewares.push(sagaMiddleware);
  // Store creation
  const store = createStore(
    rootReducer,
    applyMiddleware(...middlewares)
  )
  // Run the saga middleware
  const x = sagaMiddleware.run(rootSaga);

  return store;
}

export default storeSetup;
