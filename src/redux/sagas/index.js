import { takeLatest } from 'redux-saga/effects'
import API from '../../services/api';
import { HangmanTypes } from '../reducers/hangman';
import { getRandomWord, displayError } from './hangman';

const api = API.create();

export default function * root () {
  yield [
    takeLatest(HangmanTypes.WORD_REQUEST, getRandomWord, api),
    takeLatest(HangmanTypes.GAME_RESET, getRandomWord, api),
    takeLatest(HangmanTypes.GAME_START, getRandomWord, api),
    takeLatest(HangmanTypes.DISPLAY_ERROR, displayError)
  ]
}
