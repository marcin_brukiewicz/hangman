import { call, put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { path } from 'ramda';
import HangmanActions from '../reducers/hangman';

export function * getRandomWord (api, { previousWord }) {
  const response = yield call(api.getRandomWord)
  if (response.ok) {
    const word = path(['data', 'word'], response).toUpperCase();
    if (word != previousWord) {
      yield put(HangmanActions.wordSuccess(word))
    } else {
      yield put(HangmanActions.wordRequest(previousWord))
    }
  } else {
    yield put(HangmanActions.wordFailure())
  }
}

export function * displayError (action) {
  const { error } = action;
  if (error) {
    yield delay(2500);
    yield put(HangmanActions.displayError(null));
  }
}
