export const keyPressedFormatter = (state, char) => {
  const mappedGameWord = state.mappedGameWord.map(x => {
    if (x.letter == char) x.typed = true;
    return x;
  });
  const charIncluded = state.word.includes(char);
  const missedLetters = !charIncluded && !state.missedLetters.includes(char) ?
    [...state.missedLetters, char] :
    [...state.missedLetters];
  const caughtLetters = charIncluded && !state.caughtLetters.includes(char) ?
    [...state.caughtLetters, char] :
    [...state.caughtLetters];

  return Object.assign({}, state, {
    mappedGameWord,
    missedLetters,
    caughtLetters
  });
}
